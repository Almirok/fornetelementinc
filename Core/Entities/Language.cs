﻿
namespace Core.Entities
{
    public class Language : IdentifiableEntity<int>
    {
        protected Language()
        { }
        public string Name { get; protected set; }

        public Language( string name)
        {
            Name = name;
        }

        public Language Update(string name)
        {
            Name = name;

            return this;
        }
    }
}