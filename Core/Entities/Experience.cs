﻿
namespace Core.Entities
{
    public class Experience : IdentifiableEntity<int>
    {
        protected Experience()
        { }
        public virtual Employees Employ { get; protected set; }
        public virtual Language Language { get; protected set; }

        public Experience(Employees employ, Language language)
        {
            Employ = employ;
            Language = language;
        }

        public Experience Update(Employees employ, Language language)
        {
            Employ = employ;
            Language = language;

            return this;
        }
    }
}