﻿namespace Core.Entities
{
    public class IdentifiableEntity<TKey>
    {
        public virtual TKey Id { get; protected set; }
    }
}
