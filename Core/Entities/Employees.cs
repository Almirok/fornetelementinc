﻿
namespace Core.Entities
{
    public class Employees : IdentifiableEntity<int>
    {
        public Employees()
        { }
        public string Surname { get; protected set; }
        public string Name { get; protected set; }
        public int Age { get; protected set; }
        public string Gender { get; protected set; }
        public virtual Departments Departments { get; protected set; }
        public virtual Language Language { get; protected set; }

        public Employees(string surname, string name, int age, string gender, Departments departments, Language language)
        {
            Surname = surname;
            Name = name;
            Age = age;
            Gender = gender;
            Departments = departments;
            Language = language;
        }

        public Employees Update(string surname, string name, int age, string gender, Departments departments, Language language)
        {
            Surname = surname;
            Name = name;
            Age = age;
            Gender = gender;
            Departments = departments;
            Language = language;

            return this;
        }
    }
}