﻿
namespace Core.Entities
{
    public class Departments : IdentifiableEntity<int>
    {
        protected Departments()
        { }
        public string Name { get; protected set; }
        public int Floor { get; protected set; }

        public Departments( string name, int floor)
        {
            Name = name;
            Floor = floor;
        }

        public Departments Update(string name, int floor)
        {
            Name = name;
            Floor = floor;

            return this;
        }
    }
}