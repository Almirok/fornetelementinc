namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addLanguage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "Language_Id", c => c.Int());
            CreateIndex("dbo.Employees", "Language_Id");
            AddForeignKey("dbo.Employees", "Language_Id", "dbo.Languages", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "Language_Id", "dbo.Languages");
            DropIndex("dbo.Employees", new[] { "Language_Id" });
            DropColumn("dbo.Employees", "Language_Id");
        }
    }
}
