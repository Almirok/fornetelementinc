namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class qwe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "Gender", c => c.String());
            DropColumn("dbo.Employees", "IsMan");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employees", "IsMan", c => c.Boolean(nullable: false));
            DropColumn("dbo.Employees", "Gender");
        }
    }
}
