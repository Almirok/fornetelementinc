﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Core.Entities;
using test.Models;
using WebApplication1;

namespace test.Controllers
{
    public class EmployController : Controller
    {
        public ApplicationDbContext Repository { get; } = new ApplicationDbContext();
        DepartmentModel depModel;
        LanguageModel langModel;

        public ActionResult Index()
        {
            List<EmployModel> UserViewModels = new List<EmployModel>();

            var entities = Repository.Employees.ToList();

            foreach (var entity in entities)
            {
                var dep = Repository.Departments.SingleOrDefault(x => x.Id == entity.Departments.Id);
                var lang = Repository.Language.SingleOrDefault(x => x.Id == entity.Language.Id);
                depModel = new DepartmentModel { Id = dep.Id, Name = dep.Name, Floor = dep.Floor };
                langModel = new LanguageModel { Id = lang.Id, Name = lang.Name };
                UserViewModels.Add(new EmployModel
                {
                    Id = entity.Id,
                    Surname = entity.Surname,
                    Name = entity.Name,
                    Age = entity.Age,
                    Gender = entity.Gender,
                    Department = depModel,
                    Language = langModel
                });
            }

            return View(UserViewModels);
        }
        public ActionResult Create()
        {
            ViewBag.Departments = new SelectList(Repository.Departments, "Id", "Name");
            ViewBag.Languages = new SelectList(Repository.Language, "Id", "Name");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmployModel model)
        {
            var dep = Repository.Departments.SingleOrDefault(x => x.Id == model.Department.Id);
            var lang = Repository.Language.SingleOrDefault(x => x.Id == model.Language.Id);

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            Employees entity = new Employees(model.Surname, model.Name, model.Age, model.Gender, dep, lang);

            Repository.Employees.Add(entity);
            Repository.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var entity = Repository.Employees.Find(id);
            ViewBag.Departments = new SelectList(Repository.Departments, "Id", "Name", entity.Departments.Id);
            ViewBag.Languages = new SelectList(Repository.Language, "Id", "Name", entity.Language.Id);
            if (entity != null)
            {
                var room = Repository.Departments.SingleOrDefault(x => x.Id == entity.Departments.Id);

                var viewmodel = new EmployModel
                {
                    Id = entity.Id,
                    Surname = entity.Surname,
                    Name = entity.Name,
                    Age = entity.Age,
                    Gender = entity.Gender,
                    Department = depModel,
                    Language = langModel
                };
                return View(viewmodel);
            }
            return RedirectToAction("Index");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployModel model)
        {
            var entity = Repository.Employees.Find(model.Id);
            var dep = Repository.Departments.SingleOrDefault(x => x.Id == model.Department.Id);
            var lang = Repository.Language.SingleOrDefault(x => x.Id == model.Language.Id);

            if (entity != null)
            {
                entity.Update(model.Surname, model.Name, model.Age, model.Gender, dep, lang);
                Repository.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var entity = Repository.Employees.Find(id);
            if (entity != null)
            {
                Repository.Employees.Remove(entity);
                Repository.SaveChanges();
            }
            return RedirectToAction("Index");

        }
        [HttpPost]
        public JsonResult Name(string Prefix)
        {
            List<Employees> empls = Repository.Employees.ToList();
            var name = (from N in empls
                            where N.Name.StartsWith(Prefix)
                            select new { N.Name });
            return Json(name, JsonRequestBehavior.AllowGet);
        }
    }
}



