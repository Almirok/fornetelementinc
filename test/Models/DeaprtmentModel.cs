﻿
namespace test.Models
{
    public class DepartmentModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Floor { get; set; }
    }
}