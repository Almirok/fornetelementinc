﻿
using System.Web.Mvc;

namespace test.Models
{
    public class EmployModel
    {
        public int Id { get; set; }
        
        public string Surname { get; set; }
        [Remote("AutoComplete", "Employ")]
        public string Name { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public virtual DepartmentModel Department { get; set; }
        public virtual LanguageModel Language { get; set; }
    }
}